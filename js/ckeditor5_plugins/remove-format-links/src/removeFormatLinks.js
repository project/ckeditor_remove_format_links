import { Plugin } from 'ckeditor5/src/core';

export default class RemoveFormatLinks extends Plugin {

  init() {
    const { editor } = this;
    editor.model.schema.setAttributeProperties('linkHref', {
      isFormatting: true
    });
  }
}
