### CKEditor Remove Format Links

This module enhances CKEditor 5's Remove Format plugin by adding the ability to
remove links. It's designed exclusively for CKEditor 5, requiring no additional
setup. To deactivate the feature, simply uninstall the module.

For users operating CKEditor 4, an alternative solution worth considering is the
CKEditor Custom Config module.